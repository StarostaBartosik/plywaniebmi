package com.example.maciek.bmilab

import org.junit.Assert
import org.junit.Test
import java.lang.IllegalArgumentException

class BmiForKgCmTest {
    @Test
    fun bmi_is_correct() {
        val bmi = BmiForKgCm(60, 170)
        Assert.assertEquals(20.76, bmi.countBMI(), 0.001)
    }

    @Test
    fun bmi_return_correct_error() {
        try {
            val bmi = BmiForKgCm(30, 30)
            bmi.countBMI()
            Assert.fail()
        }
        catch (e: IllegalArgumentException){}
    }
    @Test
    fun bmi_correct_return(){
        var bmi = BmiForKgCm(90, 200)
        Assert.assertEquals("Normal", bmi.result())
        bmi = BmiForKgCm(120, 200)
        Assert.assertEquals("Obese", bmi.result())
        bmi = BmiForKgCm(140, 200)
        Assert.assertEquals("Extremely obese", bmi.result())
        bmi = BmiForKgCm(150, 200)
        Assert.assertEquals("Extremely obese", bmi.result())
    }
}