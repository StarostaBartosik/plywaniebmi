package com.example.maciek.bmilab

import org.junit.Assert
import org.junit.Test
import java.lang.IllegalArgumentException

class BmiForLbsInTest {

    @Test
    fun bmi_is_correct() {
        val bmi = BmiForLbsIn(200, 80)
        Assert.assertEquals(21.97, bmi.countBMI(), 0.001)
    }

    @Test
    fun bmi_return_correct_error() {
        try {
            val bmi = BmiForKgCm(30, 30)
            bmi.countBMI()
            Assert.fail()
        }
        catch (e: IllegalArgumentException){}
    }
}