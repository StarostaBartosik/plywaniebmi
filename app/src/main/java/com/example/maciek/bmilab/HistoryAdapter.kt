package com.example.maciek.bmilab

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.history_list_item.view.*


class HistoryAdapter(val items: ArrayList<String>, val context: Context):RecyclerView.Adapter<ViewHolder>() {



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.historyType?.text = items.get(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.history_list_item, parent, false))
    }

}
class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val historyType = view.history_type
}