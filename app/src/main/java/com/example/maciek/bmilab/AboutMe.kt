package com.example.maciek.bmilab

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Switch

class AboutMe : AppCompatActivity() {

    private var iscolor = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_me)

        val switch1 = findViewById<Switch>(R.id.switchColor)

        val layout = findViewById<LinearLayout>(R.id.about_me)

        switch1.setOnCheckedChangeListener { _, _ ->

            iscolor = if (iscolor) {
                layout.setBackgroundColor(Color.CYAN)
                false
            } else {
                layout.setBackgroundColor(Color.WHITE)
                true
            }
        }
    }
}
