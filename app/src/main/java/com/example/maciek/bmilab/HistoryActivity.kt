package com.example.maciek.bmilab

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_history.*

class HistoryActivity : AppCompatActivity() {

    val history: ArrayList<String> = ArrayList(10)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        addHistory()

        history_list.layoutManager = LinearLayoutManager(this)
        history_list.adapter = HistoryAdapter(history, this)
    }

    fun addHistory() {
        history.add("1990")
        history.add("1991")
        history.add("1992")
        history.add("1993")
        history.add("1994")
        history.add("1995")
        history.add("1996")
        history.add("1997")
        history.add("1998")
        history.add("1999")
        history.add("2000")
        history.add("2001")
        history.add("2002")
        history.add("2003")
        history.add("2004")
        history.add("2005")




    }
}
