package com.example.maciek.bmilab


import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.lang.NumberFormatException


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null) {
            findViewById<TextView>(R.id.resultString).text = savedInstanceState.getString("result")
            findViewById<TextView>(R.id.result).text = savedInstanceState.getString("bmi")

            if (findViewById<TextView>(R.id.result).text.toString() != "")
                changeColor(findViewById<TextView>(R.id.result).text.toString().toDouble())
        }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    fun sendBMI(view: View) {
        val bmi = findViewById<TextView>(R.id.result).text.toString()
        val resultString = findViewById<TextView>(R.id.resultString).text.toString()

        val intent = Intent(this, Info::class.java).apply {
            putExtra("bmi", bmi)
            putExtra("result", resultString)
        }
        startActivity(intent)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == R.id.change) {
            val mass = findViewById<EditText>(R.id.weightInput)
            val height = findViewById<EditText>(R.id.heightInput)
            val mode = findViewById<TextView>(R.id.mode)
            val result = findViewById<TextView>(R.id.result)
            val resultString = findViewById<TextView>(R.id.resultString)
            if (mode.text.toString().toInt() == 1) {
                mass.setHint(R.string.main_weight_lbs)
                height.setHint(R.string.main_height_in)
                mode.text = "2"
            } else {
                mass.setHint(R.string.main_weight_kg)
                height.setHint(R.string.main_height_cm)
                mode.text = "1"
            }
            mass.setText("")
            height.setText("")
            result.text = ""
            resultString.text = ""


        }
        if (item.itemId == R.id.about_me) {
            val i = Intent(this, AboutMe::class.java)
            startActivity(i)
        }
        if (item.itemId == R.id.hisotry) {
            val i = Intent(this, HistoryActivity::class.java)
            startActivity(i)
        }
        return true
    }

    fun count(view: View) {
        var mass = 0
        var height = 0
        try {
            mass = findViewById<EditText>(R.id.weightInput).text.toString().toInt()
            height = findViewById<EditText>(R.id.heightInput).text.toString().toInt()
        }
        catch (e: NumberFormatException){
            Toast.makeText(this, "Please give me numbers", Toast.LENGTH_SHORT).show()

        }


        val mode = findViewById<TextView>(R.id.mode)

        val bmi = if (mode.text.toString().toInt() == 1) {
            BmiForKgCm(mass, height)
        } else
            BmiForLbsIn(mass, height)

        try {

            val number = bmi.countBMI().toString()
            changeColor(number.toDouble())
            findViewById<TextView>(R.id.result).text = number
            findViewById<TextView>(R.id.resultString).text = bmi.result()

        } catch (e: IllegalArgumentException) {
            Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }


    }

    private fun changeColor(result: Double) {
        val bmi = findViewById<TextView>(R.id.result)


        when {
            result < 18.5 -> bmi.setTextColor(Color.parseColor("#00A693"))
            result < 25 -> bmi.setTextColor(Color.parseColor("#26619C"))
            result < 30 -> bmi.setTextColor(Color.parseColor("#B80000"))
            result < 35 -> bmi.setTextColor(Color.parseColor("#3300CC"))
            else -> bmi.setTextColor(Color.parseColor("#007FFF"))
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("bmi", findViewById<TextView>(R.id.result).text.toString())
        outState.putString("result", findViewById<TextView>(R.id.resultString).text.toString())

    }

    


}
