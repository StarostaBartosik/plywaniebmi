package com.example.maciek.bmilab

import kotlin.math.round

class BmiForLbsIn(var mass: Int, var height: Int): Bmi {
    override fun countBMI(): Double  {


        require(mass in 91..1000 && height in 21..100){"Please enter valid data"}

        return round(mass*703.0  / (height * height)*100)/100
    }

    override fun result(): String {
        val bmi = countBMI()

        return when {
            bmi < 18.5 -> "Underweight"
            bmi < 25 -> "Normal"
            bmi < 30 -> "Overweight"
            bmi < 35 -> "Obese"
            else -> "Extremely obese"
        }
    }
}