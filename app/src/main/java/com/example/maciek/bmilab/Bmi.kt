package com.example.maciek.bmilab

interface Bmi {
    fun countBMI():Double
    fun result():String
}